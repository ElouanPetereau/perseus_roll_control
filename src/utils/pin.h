#ifndef PIN_H
#define PIN_H

#define SWITCH_PIN 17
#define SWITCH_LED_PIN 27
#define STATUS_LED_PIN 22

#define MOSFET_ROLL_POS_1_ID 6 // Channel 1
#define MOSFET_ROLL_POS_2_ID 5 // Channel 2
#define MOSFET_ROLL_NEG_2_ID 4 // Channel 3
#define MOSFET_ROLL_NEG_1_ID 3 // Channel 4
#define MOSFET_OPEN_ARM_ID 1   // Channel 6
#define MOSFET_CLOSE_ARM_ID 2  // Channel 7

#endif // PIN_H
