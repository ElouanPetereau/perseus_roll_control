#ifndef QUEUE_H
#define QUEUE_H

#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>

#define DATA_QUEUE_SIZE 254

typedef struct {
    uint32_t packet_counter;
    uint32_t time_nav;
    float velocity_n;
    float velocity_e;
    float velocity_d;
    uint32_t time_euler;
    float yaw;
    uint32_t time_imu;
    float gyro_z;
} DataMeasurement;

typedef struct {
    DataMeasurement data[DATA_QUEUE_SIZE];
    atomic_size_t read_index;
    atomic_size_t write_index;
} LockFreeQueue;

void enqueue(LockFreeQueue *queue, DataMeasurement value);
bool dequeue(LockFreeQueue *queue, DataMeasurement *value);

#endif
