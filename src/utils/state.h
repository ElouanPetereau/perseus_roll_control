#ifndef STATE_H
#define STATE_H

#include <stdatomic.h>
#include <stdio.h>

typedef enum {
    StateUninitialized = 0,
    StateReadyForLiftoff = 1,
    StateAscending = 2,
    StateExperienceStarted = 3,
    StateExperienceStopped = 4
} ControlState;

static atomic_int ROLL_CONTROL_STATE = ATOMIC_VAR_INIT(StateUninitialized);

inline static void update_state(ControlState new_state) {
    printf("Switched state from %d to %d\n", atomic_load(&ROLL_CONTROL_STATE), new_state);
    atomic_store(&ROLL_CONTROL_STATE, new_state);
}

#endif // STATE_H
