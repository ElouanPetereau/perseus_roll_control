#include <stdio.h>
#include <stdatomic.h>
#include "queue.h"

void enqueue(LockFreeQueue* queue, DataMeasurement value) {
    size_t write_index = atomic_load_explicit(&queue->write_index, memory_order_relaxed);
    size_t read_index = atomic_load_explicit(&queue->read_index, memory_order_acquire);

    size_t next_write_index = (write_index + 1) % DATA_QUEUE_SIZE;

    if (next_write_index != read_index) {
        queue->data[write_index] = value;
        atomic_store_explicit(&queue->write_index, next_write_index, memory_order_release);
    }
}

bool dequeue(LockFreeQueue* queue, DataMeasurement* value) {
    size_t read_index = atomic_load_explicit(&queue->read_index, memory_order_relaxed);
    size_t write_index = atomic_load_explicit(&queue->write_index, memory_order_acquire);

    if (read_index == write_index) {
        return false; // Queue is empty
    }

    *value = queue->data[read_index];
    size_t next_read_index = (read_index + 1) % DATA_QUEUE_SIZE;
    atomic_store_explicit(&queue->read_index, next_read_index, memory_order_release);

    return true;
}
