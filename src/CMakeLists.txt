# GPIOD dependencies
pkg_check_modules(GPIOD REQUIRED libgpiod)
link_directories(${GPIOD_LIBRARY_DIRS})
include_directories(${GPIOD_INCLUDE_DIRS})

# Add thread-related files from the threads folder
file(GLOB THREAD_FILES threads/*.c)

# Add mosfets-related files from the subsystems folder
file(GLOB MOSFETS_FILES subsystems/*.c)

# Add queue-related files from the utils folder
file(GLOB QUEUE_FILES utils/queue.c)

add_executable(${PROJECT_NAME} 
    main.c ${THREAD_FILES} ${MOSFETS_FILES} ${QUEUE_FILES}
)
target_link_libraries(${PROJECT_NAME} PRIVATE ${GPIOD_LIBRARIES} pthread)
