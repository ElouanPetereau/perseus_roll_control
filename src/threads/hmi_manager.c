#include "hmi_manager.h"
#include "../subsystems/mosfet.h"
#include "../utils/pin.h"
#include "../utils/state.h"
#include <gpiod.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#define SWITCH_TIMEOUT 5     // Timeout in seconds
#define LED_BLINK_SPEED 500 // Led blink speed in milliseconds

void *hmi_manager(void *arg) {
    (void)arg; // Remove the unused parameter warning

    // HMI manager thread logic
    printf("HMI manager thread\n");

    struct gpiod_chip *chip;
    struct gpiod_line *switch_pin, *switch_led_pin, *status_led_pin;
    struct timespec start_time, current_time, last_blink_time;

    int status_led_state = 0, switch_led_state = 0;
    bool arm_open = true;
    bool hmi_available = true;

    // Open the GPIO chip
    chip = gpiod_chip_open("/dev/gpiochip0");
    if (!chip) {
        perror("Failed to open GPIO chip");
        return NULL;
    }

    // Open the GPIO switch_led_pin
    switch_led_pin = gpiod_chip_get_line(chip, SWITCH_LED_PIN);
    if (!switch_led_pin) {
        perror("Failed to get GPIO switch_led_pin");
        gpiod_line_release(switch_led_pin);
        gpiod_chip_close(chip);
        return NULL;
    }
    gpiod_line_request_output(switch_led_pin, "switch_led", 0);

    // Open the GPIO status_led_pin
    status_led_pin = gpiod_chip_get_line(chip, STATUS_LED_PIN);
    if (!status_led_pin) {
        perror("Failed to get GPIO status_led_pin");
        gpiod_line_release(status_led_pin);
        gpiod_chip_close(chip);
        return NULL;
    }
    gpiod_line_request_output(status_led_pin, "status_led", 0);

    // Open the GPIO switch_pin
    switch_pin = gpiod_chip_get_line(chip, SWITCH_PIN);
    if (!switch_pin) {
        perror("Failed to get GPIO switch_pin");
        gpiod_chip_close(chip);
        return NULL;
    }

    // Request the GPIO switch_pin as pull-up
    int flags = GPIOD_LINE_REQUEST_FLAG_BIAS_PULL_UP;
    if (gpiod_line_request_input_flags(switch_pin, "switch-bulk", flags) < 0) {
        perror("Failed to get GPIO switch_pin");
        gpiod_line_release(switch_pin);
        gpiod_chip_close(chip);
        return NULL;
    }

    // Get the start time
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    clock_gettime(CLOCK_MONOTONIC, &last_blink_time);

    while (hmi_available) {
        switch (atomic_load(&ROLL_CONTROL_STATE)) {
            case StateUninitialized: {
                // Check if 5 seconds have elapsed
                clock_gettime(CLOCK_MONOTONIC, &current_time);
                if ((current_time.tv_sec - start_time.tv_sec) >= SWITCH_TIMEOUT) {
                    printf("Switch timeout reached\n");

                    // Close the arm
                    if (arm_open) {
                        mosfet_write(0, MOSFET_CLOSE_ARM_ID, true);
                        mosfet_write(0, MOSFET_OPEN_ARM_ID, false);
                        arm_open = false;
                    }

                    // Change status to ready for liftoff
                    update_state(StateReadyForLiftoff);
                    continue;
                }

                // Check if the switch have been switched
                if (gpiod_line_get_value(switch_pin) == 1) {
                    // Reset the timer
                    clock_gettime(CLOCK_MONOTONIC, &start_time);

                    // Open the arm
                    if (!arm_open) {
                        mosfet_write(0, MOSFET_CLOSE_ARM_ID, false);
                        mosfet_write(0, MOSFET_OPEN_ARM_ID, true);
                        arm_open = true;
                    }

                    if (switch_led_state != 1) {
                        gpiod_line_set_value(switch_led_pin, 1);
                        switch_led_state = 1;
                    }
                } else {
                    // Close the arm
                    if (arm_open) {
                        mosfet_write(0, MOSFET_CLOSE_ARM_ID, true);
                        mosfet_write(0, MOSFET_OPEN_ARM_ID, false);
                        arm_open = false;
                    }

                    if (switch_led_state != 0) {
                        gpiod_line_set_value(switch_led_pin, 0);
                        switch_led_state = 0;
                    }
                }

                // Blink status led if the led while we are in uninitialized mode
                if ((current_time.tv_sec - last_blink_time.tv_sec) * 1000000000 +
                        (current_time.tv_nsec - last_blink_time.tv_nsec) >=
                    (LED_BLINK_SPEED * 1000000)) {
                    status_led_state = (status_led_state + 1) % 2;
                    gpiod_line_set_value(status_led_pin, status_led_state);
                    clock_gettime(CLOCK_MONOTONIC, &last_blink_time);
                }
                break;
            }
            case StateReadyForLiftoff: {
                if (switch_led_state != 0) {
                    gpiod_line_set_value(switch_led_pin, 0);
                    switch_led_state = 0;
                }
                if (status_led_state != 1) {
                    gpiod_line_set_value(status_led_pin, 1);
                    status_led_state = 1;
                }
                break;
            }
            default: {
                gpiod_line_set_value(status_led_pin, 0);
                hmi_available = false;
                break;
            }
        }

        // Sleep for 50ms
        usleep(50000);
    }

    // Release the GPIO pins
    gpiod_line_release(switch_pin);
    gpiod_line_release(switch_led_pin);
    gpiod_line_release(status_led_pin);

    // Close the GPIO chip
    gpiod_chip_close(chip);

    return NULL;
}
