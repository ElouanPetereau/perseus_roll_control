#include "data_receiver.h"
#include "../utils/queue.h"
#include "../utils/state.h"
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFFER_SIZE 512
#define CIRCULAR_BUFFER_SIZE 1024

static const char *OBC_IP = "192.168.1.6";
// static const char *ROLL_CONTROL_IP = "192.168.1.2";
#define ROLL_CONTROL_PORT 5000

typedef struct {
    char buffer[CIRCULAR_BUFFER_SIZE];
    unsigned int read_index;
    unsigned int write_index;
} CircularBuffer;

#define ROLL_MEASUREMENT_LENGTH 38

extern pthread_mutex_t START_MUTEX;
extern LockFreeQueue DATA_QUEUE;

void parse_buffer(CircularBuffer *circular_buffer, int index) {
    // Check if there is enough data in the buffer to perform the parsing
    if (circular_buffer->write_index - circular_buffer->read_index >= ROLL_MEASUREMENT_LENGTH) {
        // Check that there is an actual measurement datagram
        if (circular_buffer->buffer[(circular_buffer->read_index) % CIRCULAR_BUFFER_SIZE] == 0xAA &&
            circular_buffer->buffer[(circular_buffer->read_index + ROLL_MEASUREMENT_LENGTH - 1) %
                                    CIRCULAR_BUFFER_SIZE] == 0xFF) {

            // Try to parse a measurement datagram
            unsigned int current_index = 1;
            uint32_t packet_counter =
                *((uint32_t *)(circular_buffer->buffer +
                               (circular_buffer->read_index + current_index) % CIRCULAR_BUFFER_SIZE));
            current_index += 4;
            uint32_t time_nav =
                *((uint32_t *)(circular_buffer->buffer +
                               (circular_buffer->read_index + current_index) % CIRCULAR_BUFFER_SIZE));
            current_index += 4;
            float velocity_n =
                *((float *)(circular_buffer->buffer +
                            (circular_buffer->read_index + current_index) % CIRCULAR_BUFFER_SIZE));
            current_index += 4;
            float velocity_e =
                *((float *)(circular_buffer->buffer +
                            (circular_buffer->read_index + current_index) % CIRCULAR_BUFFER_SIZE));
            current_index += 4;
            float velocity_d =
                *((float *)(circular_buffer->buffer +
                            (circular_buffer->read_index + current_index) % CIRCULAR_BUFFER_SIZE));
            current_index += 4;
            uint32_t time_euler =
                *((uint32_t *)(circular_buffer->buffer +
                               (circular_buffer->read_index + current_index) % CIRCULAR_BUFFER_SIZE));
            current_index += 4;
            float yaw = *((float *)(circular_buffer->buffer +
                                    (circular_buffer->read_index + current_index) % CIRCULAR_BUFFER_SIZE));
            current_index += 4;
            uint32_t time_imu =
                *((uint32_t *)(circular_buffer->buffer +
                               (circular_buffer->read_index + current_index) % CIRCULAR_BUFFER_SIZE));
            current_index += 4;
            float gyro_z = *((float *)(circular_buffer->buffer +
                                       (circular_buffer->read_index + current_index) % CIRCULAR_BUFFER_SIZE));
            current_index += 4;

            DataMeasurement new_data_measurement = {.packet_counter = packet_counter,
                                                    .time_nav = time_nav,
                                                    .velocity_n = velocity_n,
                                                    .velocity_e = velocity_e,
                                                    .velocity_d = velocity_d,
                                                    .time_euler = time_euler,
                                                    .yaw = yaw,
                                                    .time_imu = time_imu,
                                                    .gyro_z = gyro_z};

            enqueue(&DATA_QUEUE, new_data_measurement);

            printf("Parsed a measurement datagram at index %d\n", index);
        }
    }
}

void *data_receiver(void *arg) {
    (void)arg; // Remove the unused parameter warning

    // Data receiver thread logic
    printf("Data receiver thread\n");

    while (1) {
        DataMeasurement new_data_measurement = {.packet_counter = 0,
                                                .time_nav = 0,
                                                .velocity_n = 0.0,
                                                .velocity_e = 0.0,
                                                .velocity_d = 0.0,
                                                .time_euler = 0,
                                                .yaw = 0.0,
                                                .time_imu = 0,
                                                .gyro_z = 0.0};

        enqueue(&DATA_QUEUE, new_data_measurement);
        printf("enqueue a data");
        usleep(1000000);
    }

    /*
    // Lock start mutex
    pthread_mutex_lock(&START_MUTEX);

    int sockfd;
    struct sockaddr_in servaddr, cliaddr;
    socklen_t len = sizeof(cliaddr);
    char buffer[BUFFER_SIZE];
    CircularBuffer circular_buffer;
    circular_buffer.read_index = 0;
    circular_buffer.write_index = 0;

    // Create UDP socket
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    // Configure server address
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(OBC_IP);
    servaddr.sin_port = htons(ROLL_CONTROL_PORT);

    // Bind the socket with the server address
    if (bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    while (1) {
        ssize_t numBytes = recvfrom(sockfd, buffer, sizeof(buffer), 0, (struct sockaddr *)&cliaddr, &len);
        if (numBytes < 0) {
            perror("Receive failed");
            exit(EXIT_FAILURE);
        }
        if (atomic_load(&ROLL_CONTROL_STATE) == StateReadyForLiftoff) {
            pthread_mutex_unlock(&START_MUTEX);
            update_state(StateAscending);
            printf("Ready For Liftoff!");
        }

        for (int i = 0; i < numBytes; i++) {
            // Write data to circular buffer
            circular_buffer.buffer[circular_buffer.write_index] = buffer[i];
            circular_buffer.write_index = (circular_buffer.write_index + 1) % CIRCULAR_BUFFER_SIZE;

            // Check for incoming data
            parse_buffer(&circular_buffer, circular_buffer.write_index);
        }
    }

    // Close the socket
    close(sockfd);
    */

    return NULL;
}
