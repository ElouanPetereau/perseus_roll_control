#include "period_manager.h"
#include "../utils/state.h"
#include "data_receiver.h"
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#define ROLL_CONTROL_START_TIMEOUT 16 // Roll control start timer after liftoff (in s)
#define ROLL_CONTROL_STOP_TIMEOUT                                                                            \
    (ROLL_CONTROL_START_TIMEOUT + 2) // Roll control stop timer after liftoff (in s)
#define UPDATE_LAW_TIMEOUT 5         // Update law timer (in ms)

extern pthread_mutex_t START_MUTEX;

void *period_manager(void *arg) {
    // Period manager thread logic
    printf("Period manager thread\n");

    struct timespec exp_start_time, update_law_time, current_time;
    long unsigned roll_controller_thread_id = *(pthread_t *)arg;
    bool experience_started = false, experience_stopped = false;

    // Lock onto the START_MUTEX
    pthread_mutex_lock(&START_MUTEX);

    clock_gettime(CLOCK_MONOTONIC, &exp_start_time);
    clock_gettime(CLOCK_MONOTONIC, &update_law_time);

    while (1) {
        clock_gettime(CLOCK_MONOTONIC, &current_time);

        // Start the experience after ROLL_CONTROL_START_TIMEOUT time and stop it after
        // ROLL_CONTROL_STOP_TIMEOUT only if we are in StateAscending
        if (atomic_load(&ROLL_CONTROL_STATE) == StateAscending) {
            if (!experience_started &&
                ((current_time.tv_sec - exp_start_time.tv_sec) >= ROLL_CONTROL_START_TIMEOUT)) {
                pthread_kill(roll_controller_thread_id, START_STOP_EXPERIENCE_SIGNAL);
                experience_started = true;
            }
            if (!experience_stopped &&
                ((current_time.tv_sec - exp_start_time.tv_sec) >= ROLL_CONTROL_STOP_TIMEOUT)) {
                pthread_kill(roll_controller_thread_id, START_STOP_EXPERIENCE_SIGNAL);
                experience_stopped = true;
            }
        }

        if ((current_time.tv_sec - update_law_time.tv_sec) * 1000000000 +
                (current_time.tv_nsec - update_law_time.tv_nsec) >=
            (UPDATE_LAW_TIMEOUT * 1000000)) {
            pthread_kill(roll_controller_thread_id, UPDATE_LAW_SIGNAL);
            clock_gettime(CLOCK_MONOTONIC, &update_law_time);
        }

        usleep(1000);
    }

    return NULL;
}
