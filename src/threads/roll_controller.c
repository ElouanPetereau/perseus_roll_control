#include "roll_controller.h"
#include "../utils/queue.h"
#include "period_manager.h"
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdatomic.h>

extern LockFreeQueue DATA_QUEUE;

atomic_bool received_signal_update_law = ATOMIC_VAR_INIT(false);
atomic_bool received_signal_start_stop_experience = ATOMIC_VAR_INIT(false);
bool expected = true;

void period_signal_handler(int signal) {
    switch (signal) {
        case UPDATE_LAW_SIGNAL: {
            atomic_store(&received_signal_update_law, true);
            break;
        }
        case START_STOP_EXPERIENCE_SIGNAL: {
            atomic_store(&received_signal_start_stop_experience, true);
            break;
        }
        default:
            printf("Received an unsupported signal\n");
            break;
    }
}

void *roll_controller(void *arg) {
    (void)arg; // Remove the unused parameter warning
    
    // Roll controller thread logic
    printf("Roll controller thread\n");

    bool experience_running = false;

    // Register signals
    if (signal(UPDATE_LAW_SIGNAL, period_signal_handler) == SIG_ERR) {
        perror("Failed to register signal handler for UPDATE_LAW_SIGNAL");
    }
    if (signal(START_STOP_EXPERIENCE_SIGNAL, period_signal_handler) == SIG_ERR) {
        perror("Failed to register signal handler for START_STOP_EXPERIENCE_SIGNAL");
    }

    while (1) {
        // Check if we need to update the law
        // FIXME: atomic_compare_exchange_strong was not working so we use load then store, the data might be
        // changed between the 2 instruction
        if (atomic_load(&received_signal_update_law) == true) {
            atomic_store(&received_signal_update_law, false);
            printf("Updating the law\n");

            // TODO: dequeue the data and mean them
            DataMeasurement data;
            if (dequeue(&DATA_QUEUE, &data)) {
                printf("Dequeued: packet_counter=%d, time_nav=%d, velocity_n=%.1f, velocity_e=%.1f, "
                       "velocity_d=%.1f, time_euler=%d, yaw=%.1f, time_imu=%d, gyro_z=%.1f\n",
                       data.packet_counter, data.time_nav, data.velocity_n, data.velocity_e, data.velocity_d,
                       data.time_euler, data.yaw, data.time_imu, data.gyro_z);
            }

            // TODO: use mosfets if experience is running
        }

        // Check if the experience status as changed
        // FIXME: atomic_compare_exchange_strong was not working so we use load then store, the data might be
        // changed between the 2 instruction
        if (atomic_load(&received_signal_start_stop_experience) == true) {
            atomic_store(&received_signal_start_stop_experience, false);
            printf("Experience switched from %s to %s \n", experience_running ? "RUNNING" : "STOPPED",
                   !experience_running ? "RUNNING" : "STOPPED");
            experience_running = !experience_running;

            // TODO: init experience
        }
    }
    return NULL;
}
