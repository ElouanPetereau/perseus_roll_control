#ifndef PERIOD_MANAGER_H
#define PERIOD_MANAGER_H

#include <signal.h>

// Custom signals
#define UPDATE_LAW_SIGNAL SIGUSR1
#define START_STOP_EXPERIENCE_SIGNAL SIGUSR2

void *period_manager(void *arg);

#endif // PERIOD_MANAGER_H
