#include "threads/data_receiver.h"
#include "threads/hmi_manager.h"
#include "threads/period_manager.h"
#include "threads/roll_controller.h"
#include "utils/state.h"
#include "utils/queue.h"
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

// Global queue that store the received data
LockFreeQueue DATA_QUEUE;

// Global mutex to know when to start the period manager
pthread_mutex_t START_MUTEX;

int main() {
    update_state(StateUninitialized);

    // Initialize the start mutex
    pthread_mutex_init(&START_MUTEX, NULL);

    // Initialize the queue
    atomic_init(&DATA_QUEUE.read_index, 0);
    atomic_init(&DATA_QUEUE.write_index, 0);

    pthread_t data_receiver_thread, period_manager_thread, roll_controller_thread, hmi_manager_thread;

    // Create threads
    pthread_create(&data_receiver_thread, NULL, data_receiver, NULL);
    // Sleep for 500ms to give the data_receiver_thread the time to lock onto the START_MUTEX
    usleep(500000);
    pthread_create(&roll_controller_thread, NULL, roll_controller, NULL);
    // Sleep for 500ms to give the roll_controller_thread the time to setup the signal blocking waiter and not
    // use the default handler that cause the program to terminate
    usleep(500000);
    pthread_create(&period_manager_thread, NULL, period_manager, &roll_controller_thread);
    pthread_create(&hmi_manager_thread, NULL, hmi_manager, NULL);

    // Wait for threads to finish
    pthread_join(data_receiver_thread, NULL);
    pthread_join(period_manager_thread, NULL);
    pthread_join(roll_controller_thread, NULL);
    pthread_join(hmi_manager_thread, NULL);

    pthread_mutex_destroy(&START_MUTEX);

    return 0;
}
