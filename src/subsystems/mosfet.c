#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "comm.h"
#include "mosfet.h"

#define VERSION_BASE (int)1
#define VERSION_MAJOR (int)0
#define VERSION_MINOR (int)0

#define UNUSED(X) (void)X /* To avoid gcc/g++ warnings */
#define CMD_ARRAY_SIZE 7

const uint8_t MOSFET_MASK_REMAP[8] = {0x20, 0x40, 0x08, 0x04, 0x02, 0x01, 0x80, 0x10};
const int MOSFET_CHANNEL_REMAP[8] = {5, 6, 3, 2, 1, 0, 7, 4};

uint8_t mosfet_to_io(uint8_t mosfet) {
    uint8_t i;
    uint8_t val = 0;
    for (i = 0; i < 8; i++) {
        if ((mosfet & (1 << i)) != 0)
            val += MOSFET_MASK_REMAP[i];
    }
    return 0xff ^ val;
}

uint8_t io_to_mosfet(uint8_t io) {
    uint8_t i;
    uint8_t val = 0;

    io ^= 0xff;
    for (i = 0; i < 8; i++) {
        if ((io & MOSFET_MASK_REMAP[i]) != 0) {
            val += 1 << i;
        }
    }
    return val;
}

int mosfet_set_channel(int dev, uint8_t channel, OutStateEnumType state) {
    int resp;
    uint8_t buff[2];

    if ((channel < CHANNEL_NR_MIN) || (channel > MOSFET_CH_NR_MAX)) {
        printf("Invalid mosfet nr!\n");
        return ERROR;
    }
    if (FAIL == i2c_read_mem8(dev, MOSFET8_OUTPORT_REG_ADD, buff, 1)) {
        return FAIL;
    }

    switch (state) {
        case ON:
            buff[0] &= ~(1 << MOSFET_CHANNEL_REMAP[channel - 1]);
            resp = i2c_write_mem8(dev, MOSFET8_OUTPORT_REG_ADD, buff, 1);
            break;
        case OFF:
            buff[0] |= 1 << MOSFET_CHANNEL_REMAP[channel - 1];
            resp = i2c_write_mem8(dev, MOSFET8_OUTPORT_REG_ADD, buff, 1);
            break;
        default:
            printf("Invalid mosfet state!\n");
            return ERROR;
            break;
    }
    return resp;
}

int mosfet_get_channel(int dev, uint8_t channel, OutStateEnumType *state) {
    uint8_t buff[2];

    if (NULL == state) {
        return ERROR;
    }

    if ((channel < CHANNEL_NR_MIN) || (channel > MOSFET_CH_NR_MAX)) {
        printf("Invalid mosfet nr!\n");
        return ERROR;
    }

    if (FAIL == i2c_read_mem8(dev, MOSFET8_OUTPORT_REG_ADD, buff, 1)) {
        return ERROR;
    }

    if (buff[0] & (1 << MOSFET_CHANNEL_REMAP[channel - 1])) {
        *state = OFF;
    } else {
        *state = ON;
    }
    return OK;
}

int mosfet_set(int dev, int val) {
    uint8_t buff[2];

    buff[0] = mosfet_to_io(0xff & val);

    return i2c_write_mem8(dev, MOSFET8_OUTPORT_REG_ADD, buff, 1);
}

int mosfet_get(int dev, int *val) {
    uint8_t buff[2];

    if (NULL == val) {
        return ERROR;
    }
    if (FAIL == i2c_read_mem8(dev, MOSFET8_OUTPORT_REG_ADD, buff, 1)) {
        return ERROR;
    }
    *val = io_to_mosfet(buff[0]);
    return OK;
}

int mosfet_board_init(int stack) {
    uint8_t buff[8];

    if ((stack < 0) || (stack > 7)) {
        printf("Invalid stack level [0..7]!");
        return ERROR;
    }

    int add = (MOSFET8_HW_I2C_BASE_ADD - stack);

    int dev = i2c_init(add);
    if (dev == -1) {
        return ERROR;
    }
    if (ERROR == i2c_read_mem8(dev, MOSFET8_CFG_REG_ADD, buff, 1)) {
        printf("8-MOSFETS card id %d not detected\n", stack);
        return ERROR;
    }
    if (buff[0] != 0) // non initialized I/O Expander
    {
        // make all I/O pins output
        buff[0] = 0;
        if (0 > i2c_write_mem8(dev, MOSFET8_CFG_REG_ADD, buff, 1)) {
            return ERROR;
        }
        // put all pins in 0-logic state
        buff[0] = 0xff;
        if (0 > i2c_write_mem8(dev, MOSFET8_OUTPORT_REG_ADD, buff, 1)) {
            return ERROR;
        }
    }

    return dev;
}

/**
 * mosfet_write:
 * - board_id: level id of the board (0-7)
 * - mosfet_id: channel id of the mosfet (0-7)
 * - mosfet_state: state of the mosfet (on/off)
 */
void mosfet_write(int board_id, int mosfet_id, bool mosfet_state) {
    OutStateEnumType state, stateR = STATE_COUNT;

    int dev = mosfet_board_init(board_id);
    if (dev <= 0) {
        exit(EXIT_FAILURE);
    }
    if (mosfet_state == true) {
        state = ON;
    } else {
        state = OFF;
    }

    int retry = RETRY_TIMES;
    while ((retry > 0) && (stateR != state)) {
        if (OK != mosfet_set_channel(dev, mosfet_id, state)) {
            printf("Failed to write mosfet\n");
            exit(EXIT_FAILURE);
        }
        if (OK != mosfet_get_channel(dev, mosfet_id, &stateR)) {
            printf("Failed to read mosfet\n");
            exit(EXIT_FAILURE);
        }
        retry--;
    }

    if (retry == 0) {
        printf("Failed to write mosfet after %d attempts\n", RETRY_TIMES);
        exit(EXIT_FAILURE);
    }
}

/**
 * mosfet_read:
 * - board_id: level id of the board (0-7)
 * - mosfet_id: channel id of the mosfet (0-7)
 *
 *  return: the mosfet state (ON -> true, OFF -> false)
 */
bool mosfet_read(int board_id, int mosfet_id) {
    OutStateEnumType state = STATE_COUNT;

    int dev = mosfet_board_init(board_id);
    if (dev <= 0) {
        exit(EXIT_FAILURE);
    }

    if ((mosfet_id < CHANNEL_NR_MIN) || (mosfet_id > MOSFET_CH_NR_MAX)) {
        printf("Mosfet number value out of range!\n");
        exit(EXIT_FAILURE);
    }

    if (OK != mosfet_get_channel(dev, mosfet_id, &state)) {
        printf("Fail to read!\n");
        exit(EXIT_FAILURE);
    }
    if (state != 0) {
        return true;
    } else {
        return false;
    }
}

int check_board(int hwAdd) {
    int dev = 0;
    uint8_t buff[8];

    hwAdd ^= 0x07;
    dev = i2c_init(hwAdd);
    if (dev == -1) {
        return FAIL;
    }
    if (ERROR == i2c_read_mem8(dev, MOSFET8_CFG_REG_ADD, buff, 1)) {
        return ERROR;
    }
    return OK;
}

void mosfet_list() {
    int ids[8];
    int i;
    int cnt = 0;

    for (i = 0; i < 8; i++) {
        if (check_board(MOSFET8_HW_I2C_BASE_ADD - i) == OK) {
            ids[cnt] = i;
            cnt++;
        }
    }
    printf("%d board(s) detected\n", cnt);
    if (cnt > 0) {
        printf("Id:");
    }
    while (cnt > 0) {
        cnt--;
        printf(" %d", ids[cnt]);
    }
    printf("\n");
}