#ifndef COMM_H_
#define COMM_H_

#include <stdint.h>

int i2c_init(int addr);
int i2c_read_mem8(int dev, int add, uint8_t *buff, int size);
int i2c_write_mem8(int dev, int add, uint8_t *buff, int size);

#endif // COMM_H_
