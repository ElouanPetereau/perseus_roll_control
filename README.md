# PERSEUS ROLL CONTROL <!-- omit in toc -->

Gitlab repository of the roll control system for the PERSEUS rocket.

---

- [Project dependencies](#project-dependencies)
- [Building the project](#building-the-project)
    - [Requirement](#requirement)

---
## Project dependencies
- libgpiod (>= 1.6.2)


## Building the project

#### Requirement
- cmake (>= v3.21)

__Create the build folder: :__
```
mkdir build
cd build
```
__Configure the project :__
- For a native compilation
    ```
    cmake ..
    ```
- For a cross compilation using the custom toolchain (see `./toolchain/README.md for more information about it)
    ```
    cmake -DCMAKE_TOOLCHAIN_FILE="../toolchain/rpizero2.cmake" ..
    ```

__Compile the project :__
```
make
```

> The compiled binaries are available in `./build/bin/`
