# TOOLCHAIN <!-- omit in toc -->

This folder contains the toolchain built for the physical board used.
It is a Raspberry Pi zero 2 W with a 1 GHz quad-core 64-bit Arm Cortex-A53 CPU.

---
- [Extract the toolchain](#extract-the-toolchain)
- [Generating the toolchain](#generating-the-toolchain)
    - [Adding gpiod library to the toolchain](#adding-gpiod-library-to-the-toolchain)
      - [Download and extract the `1.6.2` version of gpiod library](#download-and-extract-the-162-version-of-gpiod-library)
      - [Cross-compile the gpiod library using the generated toolchain](#cross-compile-the-gpiod-library-using-the-generated-toolchain)
      - [Copy the gpiod library files to the toolchain](#copy-the-gpiod-library-files-to-the-toolchain)
---

## Extract the toolchain
The first thing to do before compiling the project is to extract the toolchain from the compressed archive:
```
tar -xzf aarch64-rpizero2-linux-gnu.tar.gz
```

The toolchain is now ready to be used!

## Generating the toolchain

The toolchain was generated using the open source tool [croostool-ng](https://crosstool-ng.github.io/) that support many architectures and has a menuconfig-style interface.

Since the target board we are using is close to the Raspberry Pi 3, the toolchain was built based on this one:
```
mkdir toolchain_build
cd toolchain_build
ct-ng aarch64-rpi3-linux-gnu
```

Then open the menuconfig to start the configuration:
```
ct-ng menuconfig
```
Make the following changes:
1. Allow extending the toolchain after it is created (by default, it is created as read-only):
    Paths and misc options -> Render the toolchain read-only -> false
2. Change the tuple’s vendor string:
    Toolchain options -> Tuple’s vendor string -> rpizero2
3. Change the g-lib version to 2.31:
    C-library -> Version of glibc -> 2.31

The toolchain is ready to be built (it can be long depending on the available ressources ~45min):
```
ct-ng build
```
> The toolchain will be named aarch64-rpizero2-linux-gnu and created in ${HOME}/x-tools/aarch64-rpizero2-linux-gnu.

#### Adding gpiod library to the toolchain
As the gpiod library is a dependency of the project, it need to be added to the toolchain.

##### Download and extract the `1.6.2` version of gpiod library

```
wget https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/snapshot/libgpiod-1.6.2.tar.gz 
tar -xzf libgpiod-1.6.2.tar.gz 
cd libgpiod-1.6.2/ 
```

##### Cross-compile the gpiod library using the generated toolchain
```
export PATH=$PATH:${HOME}/x-tools/aarch64-rpizero2-linux-gnu/bin
./autogen.sh --enable-tools=yes --host=aarch64-rpizero2-linux-gnu CC=aarch64-rpizero2-linux-gnu-gcc --prefix=$(pwd)/build
ac_cv_func_malloc_0_nonnull=yes
```
> Setting ac_cv_func_malloc_0_nonnull=yes will prevent an undefined reference to `rpl_malloc` error at linking
```
make install
```

##### Copy the gpiod library files to the toolchain
```
cp -r build/*  ${HOME}/x-tools/aarch64-rpizero2-linux-gnu/
```