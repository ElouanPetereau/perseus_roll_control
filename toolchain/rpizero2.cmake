# Target operating system name
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR ARM)

# Set toolchain name
set(TOOLCHAIN_PREFIX aarch64-rpizero2-linux-gnu)
# Set toolchain dir
set(TOOLCHAIN_PATH ./toolchain/${TOOLCHAIN_PREFIX}/)
get_filename_component(TOOLCHAIN_FULL_PATH ${TOOLCHAIN_PATH} REALPATH)
#Set toolchain binutils dir
set(BINUTILS_PATH ${TOOLCHAIN_FULL_PATH}/bin/)

# Set compilers and linkers
set(CMAKE_C_COMPILER ${BINUTILS_PATH}${TOOLCHAIN_PREFIX}-gcc)
message(STATUS ${CMAKE_C_COMPILER})
set(CMAKE_ASM_COMPILER ${BINUTILS_PATH}${CMAKE_C_COMPILER})
set(CMAKE_CXX_COMPILER ${BINUTILS_PATH}${TOOLCHAIN_PREFIX}-g++)
set(CMAKE_LINKER  ${BINUTILS_PATH}${TOOLCHAIN_PREFIX}-ld)
set(CMAKE_OBJCOPY ${BINUTILS_PATH}${TOOLCHAIN_PREFIX}-objcopy CACHE INTERNAL "")
set(CMAKE_RANLIB ${BINUTILS_PATH}${TOOLCHAIN_PREFIX}-ranlib CACHE INTERNAL "")
set(CMAKE_SIZE ${BINUTILS_PATH}${TOOLCHAIN_PREFIX}-size CACHE INTERNAL "")
set(CMAKE_STRIP ${BINUTILS_PATH}${TOOLCHAIN_PREFIX}-strip CACHE INTERNAL "")

# Compile the cmake test executable as static
set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY")

# Cmake find search path
set(CMAKE_FIND_ROOT_PATH ${TOOLCHAIN_FULL_PATH})
# orce cmake to only search programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# Force cmake to only search headers and libraries in the toolchain
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
# Force cmake to only search package in the toolchain
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# Add the toolchain to the pkg-config search path
set(CMAKE_PREFIX_PATH ${CMAKE_FIND_ROOT_PATH})
# Automatically use the cross-wrapper for pkg-config
#set(PKG_CONFIG_EXECUTABLE "${TOOLCHAIN_PREFIX}-pkg-config" CACHE FILEPATH "pkg-config executable")

